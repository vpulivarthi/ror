from selenium import webdriver

class Browser(object):
    #baser_url = "http://localhost:8000"
    base_url = "http://www.cinegenicmedia.com/"
    driver = webdriver.Chrome()
    driver.implicitly_wait(10)

    def close(self):
        """
        close the webdriver instance
        """

    def visit(self,location=''):
        """
        navigate webdriver to different pages
        """

    def find_by_id(self,selector):
        """
        find a page element in the DOM
        """
        return self.driver.find_element_by_id(selector)
